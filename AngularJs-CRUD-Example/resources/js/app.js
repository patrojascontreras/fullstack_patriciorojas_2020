'use strict';
var myApp = angular.module('myApp', []);

myApp.controller('crudExampleController', ['$scope', '$http', function($scope, $http) {

    $scope.title_persons_management_gral = 'CRUD Gestión de Personas';
	$scope.title_persons_management_list = 'Listado de Personas';
	
	$scope.button_title_new_person_create = 'Nueva Persona';
	$scope.button_title_person_search = 'Buscar';
	
	$scope.grid_table_person_id = 'ID';
	$scope.grid_table_person_name = 'Nombre';
	$scope.grid_table_person_lastName = 'Apellidos';
	$scope.grid_table_person_male = 'Sexo';
	$scope.grid_table_person_email = 'Email';
	$scope.grid_table_person_category = 'Categoría';
	$scope.grid_table_person_actions = 'Acciones';

    $scope.searchingId = '';

    $scope.personIdDelete = 0;

    $scope.formPersonCreate = {
        name: '', 
        lastName: '', 
        male: '', 
        email: '', 
        category: 0
    };

    $scope.formPersonEdit = {
        idPerson: 0,
        name: '', 
        lastName: '', 
        male: '', 
        email: '', 
        category: 0
    };

    $scope.formPersonCancelBtn = 'Cancelar';
    $scope.formPersonCreateBtn = 'Registrar';
    $scope.formPersonUpdateBtn = 'Modificar';
    $scope.formPersonDeleteBtn = 'Eliminar';
    $scope.formPersonDetailsCloseBtn = 'Cerrar';

    $scope.titleFormPersonCreate = 'Formulario Registro Persona';
    $scope.titleFormPersonUpdate = 'Formulario Edición Persona';
    $scope.titleFormPersonDelete = 'Confirmar Eliminar';
    $scope.titleFormPersonDetails = 'Detalles de Persona';

    $scope.textDescriptionModalDeletePerson = '¿Desea eliminar este registro?';

    $scope.formPersonId = 'ID';
    $scope.formPersonName = 'Nombre';
    $scope.formPersonLastName = 'Apellidos';
    $scope.formPersonMale = 'Sexo';
    $scope.formPersonEmail = 'Email';
    $scope.formPersonCategory = 'Categoría';

    var urlBase = 'http://localhost:8080/SpringBoot-CRUD-Example/api';

    var specificalUrl = {
        'urlReadPersonsListAll': urlBase + '/persons/read/innerjoin/list', 
        'urlPersonCreate': urlBase + '/persons/create',
        'urlPersonUpdate': urlBase + '/persons/update', 
        'urlPersonDelete': urlBase + '/persons/delete', 
        'urlReadPersonEdit': urlBase + '/persons/read/edit', 
        'urlReadPersonDetails': urlBase + '/persons/read/persondetails', 
        'urlReadCategoriesListAll': urlBase + '/categories/read/list'
    };

    $scope.messageResponse = '';
    $scope.errorMessageResponse = '';

    //Variables respuestas de campos y/o combobox inválidos
    $scope.errorMessageInvalidName = 'Por favor, ingrese un nombre válido';
    $scope.errorMessageInvalidLastName = 'Por favor, ingrese un apellido(s) válido';
    $scope.errorMessageInvalidEmail = 'Por favor, ingrese un email válido';

    $scope.errorMessageMinLengthName = 'El campo Nombre debe tener al menos 2 letras';
    $scope.errorMessageMinLengthLastName = 'El campo Apellido(s) debe tener al menos 2 letras';
    //Fín Variables respuestas de campos y/o combobox inválidos

    $scope.prsListAll = [];
    $scope.catsListAll = [];
	
	$scope.personsListAll = function() {
		$http.get(specificalUrl.urlReadPersonsListAll).success(function(data) {
            $scope.prsListAll = data;
            console.log(data);
        }).error(function(data, status) {
            console.log("Hola mundo " + status);

            if(status == 404) {
                $scope.prsListAll = [];
                $scope.messageResponse = data.message;
                console.log($scope.messageResponse);
                $scope.searchingId = '';
            } else {
                $scope.prsListAll = [];
                $scope.errorMessageResponse = data.message;
                console.log($scope.errorMessageResponse);
                $scope.searchingId = '';
            }
        });
	};

    $scope.personsListAllById = function() {
        $http.get(specificalUrl.urlReadPersonsListAll + '/' + $scope.searchingId).success(function(data) {
            $scope.prsListAll = data;
            console.log(data);

            $scope.searchingId = '';
        }).error(function(data, status) {
            console.log("Hola mundo " + status);

            if(status == 404) {
                $scope.prsListAll = [];
                $scope.messageResponse = data.message;
                console.log($scope.messageResponse);
                $scope.searchingId = '';
            } else {
                $scope.prsListAll = [];
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
                $scope.searchingId = '';
            }
        });
    };

    $scope.categoriesListAll = function() {
        $http.get(specificalUrl.urlReadCategoriesListAll).success(function(data) {
            $scope.catsListAll = data;
            console.log(data);
        }).error(function(data, status) {

            if(status == 404) {
                $scope.catsListAll = [];
                $scope.messageResponse = data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else {
                $scope.catsListAll = [];
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.openFormPersonCreate = function() {
        $scope.clearInputsFormPersonCreate();
        $scope.categoriesListAll();
        $('#modalFormPersonCreate').modal('show');
    };

    $scope.cancelPersonCreate = function() {
        $('#modalFormPersonCreate').modal('hide');
    };

    $scope.personCreate = function() {

        var personInsert = {
            name: $scope.formPersonCreate.name, 
            lastName: $scope.formPersonCreate.lastName, 
            male: $scope.formPersonCreate.male, 
            email: $scope.formPersonCreate.email, 
            personCategory: $scope.formPersonCreate.category
        };

        $http.post(specificalUrl.urlPersonCreate, personInsert).success(function(data, status) {
            console.log(data);

            if(status == 201) {
                $scope.messageResponse = 'Dato registrado éxitosamente';
                
                $scope.personsListAll();
                toastr.success($scope.messageResponse, "Operación éxitosa");
                console.log($scope.messageResponse);
                $('#modalFormPersonCreate').modal('hide');
            } else if(status == 200) {
                console.log($scope.messageResponse);
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        }).error(function(data, status) {
            
            $scope.errorMessageResponse = data.message;
            toastr.error($scope.errorMessageResponse, "Operación erronea");
            console.log(status);
            console.log($scope.errorMessageResponse);
        });
    };

    $scope.clearInputsFormPersonCreate = function() {
        $scope.formPersonCreate.name = '';
        $scope.formPersonCreate.lastName = '';
        $scope.formPersonCreate.male = '';
        $scope.formPersonCreate.email = '';
        $scope.formPersonCreate.category = 0;

        $scope.disabledGetName = true;
        $scope.disabledGetLastName = true;
        $scope.disabledGetEmail = true;
    };

    $scope.personEdit = function(idPerson) {
        $http.get(specificalUrl.urlReadPersonEdit + '/' + idPerson).success(function(data) {
            console.log(data);

            $scope.disabledGetName = true;
            $scope.disabledGetLastName = true;
            $scope.disabledGetEmail = true;

            $scope.formPersonEdit.idPerson = data.idPerson;
            $scope.formPersonEdit.name = data.name;
            $scope.formPersonEdit.lastName = data.lastName;
            $scope.formPersonEdit.male = data.male;
            $scope.formPersonEdit.email = data.email;
            $scope.formPersonEdit.category = "" + data.personCategory + "";

            $scope.idPersonAntidisabled = data.idPerson;

            $scope.categoriesListAll();
            $('#modalFormPersonEdit').modal('show');
        }).error(function(data, status) {

            if(status == 404) {
                $scope.messageResponse = data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else {
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.personUpdate = function() {

        var personUpdated = {
            idPerson: $scope.idPersonAntidisabled,
            name: $scope.formPersonEdit.name, 
            lastName: $scope.formPersonEdit.lastName, 
            male: $scope.formPersonEdit.male, 
            email: $scope.formPersonEdit.email, 
            personCategory: $scope.formPersonEdit.category
        };

        $http.put(specificalUrl.urlPersonUpdate, personUpdated).success(function(data) {
            console.log(data);

            $scope.messageResponse = 'Dato modificado éxitosamente';

            $scope.personsListAll();
            toastr.success($scope.messageResponse, "Operación éxitosa");
            console.log($scope.messageResponse);
            $('#modalFormPersonEdit').modal('hide');
        }).error(function(data, status) {

            if(status == 404) {
                $scope.messageResponse = data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else {
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.cancelPersonUpdate = function() {
        $('#modalFormPersonEdit').modal('hide');
    };

    $scope.openModalPersonDelete = function(idPerson) {
        $scope.personIdDelete = idPerson;
        console.log($scope.personIdDelete);
        $('#modalFormPersonDelete').modal('show');
    };

    $scope.confirmPersonDelete = function() {
        $http.delete(specificalUrl.urlPersonDelete + '/' + $scope.personIdDelete).success(function(data) {
            console.log(data);

            $scope.messageResponse = 'Dato eliminado éxitosamente';

            $scope.personsListAll();
            toastr.success($scope.messageResponse, "Operación éxitosa");
            console.log($scope.messageResponse);
            $('#modalFormPersonDelete').modal('hide');
        }).error(function(data, status) {

            if(status == 404) {
                $scope.messageResponse = data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else {
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.cancelPersonDelete = function() {
        $('#modalFormPersonDelete').modal('hide');
    };

    $scope.openModalPersonDetails = function(idPerson) {
        $http.get(specificalUrl.urlReadPersonDetails + '/' + idPerson).success(function(data) {
            $scope.personDetails = data;
            console.log($scope.personDetails);
            $('#modalFormPersonReadDetails').modal('show');
        }).error(function(data, status) {

            if(status == 404) {
                $scope.messageResponse = data.message;
                toastr.error($scope.messageResponse, "Operación erronea");
                console.log($scope.messageResponse);
            } else {
                $scope.errorMessageResponse = data.message;
                toastr.error($scope.errorMessageResponse, "Operación erronea");
                console.log($scope.errorMessageResponse);
            }
        });
    };

    $scope.closeModalPersonDetails = function() {
        $('#modalFormPersonReadDetails').modal('hide');
    };

    //Funciones para validar determinados campos
    var namePattern = /^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s]+$/;
    var lastNamePattern = /^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\s]+$/;
    var emailPattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    $scope.disabledGetName = true;
    $scope.disabledGetLastName = true;
    $scope.disabledGetEmail = true;

    $scope.disabledInvalidPatternName = true;
    $scope.disabledInvalidPatternLastName = true;
    $scope.disabledInvalidPatternEmail = true;

    $scope.disabledMinlengthName = true;
    $scope.disabledMinLengthLastName = true;
    
    $scope.getInvalidName = function() {
        $scope.disabledGetName = false;

        if($scope.formPersonCreate.name.length < 2) {
            $scope.disabledMinlengthName = false;
        } else {
            $scope.disabledMinlengthName = true;
        }

        if(!namePattern.test($scope.formPersonCreate.name)) {
            $scope.disabledInvalidPatternName = false;
        } else {
            $scope.disabledInvalidPatternName = true;
            $scope.disabledGetName = true;
        }

    };

    $scope.getInvalidLastName = function() {
        $scope.disabledGetLastName = false;

        if($scope.formPersonCreate.lastName.length < 2) {
            $scope.disabledMinlengthLastName = false;
        } else {
            $scope.disabledMinlengthLastName = true;
        }

        if(!lastNamePattern.test($scope.formPersonCreate.lastName)) {
            $scope.disabledInvalidPatternLastName = false;
        } else {
            $scope.disabledInvalidPatternLastName = true;
            $scope.disabledGetLastName = true;
        }

    };

    $scope.getInvalidEmail = function() {
        $scope.disabledGetEmail = false;

        if(!emailPattern.test($scope.formPersonCreate.email)) {
            $scope.disabledInvalidPatternEmail = false;
        } else {
            $scope.disabledInvalidPatternEmail = true;
            $scope.disabledGetEmail = true;
        }
    };

    $scope.getInvalidNamePersonEdit = function() {
        $scope.disabledGetName = false;

        if($scope.formPersonEdit.name.length < 2) {
            $scope.disabledMinlengthName = false;
        } else {
            $scope.disabledMinlengthName = true;
        }

        if(!namePattern.test($scope.formPersonEdit.name)) {
            $scope.disabledInvalidPatternName = false;
        } else {
            $scope.disabledInvalidPatternName = true;
            $scope.disabledGetName = true;
        }
    };

    $scope.getInvalidLastNamePersonEdit = function() {
        $scope.disabledGetLastName = false;

        if($scope.formPersonEdit.lastName.length < 2) {
            $scope.disabledMinlengthLastName = false;
        } else {
            $scope.disabledMinlengthLastName = true;
        }

        if(!namePattern.test($scope.formPersonEdit.lastName)) {
            $scope.disabledInvalidPatternLastName = false;
        } else {
            $scope.disabledInvalidPatternLastName = true;
            $scope.disabledGetLastName = true;
        }
    };

    $scope.getInvalidEmailPersonEdit = function() {
        $scope.disabledGetEmail = false;

        if(!emailPattern.test($scope.formPersonEdit.email)) {
            $scope.disabledInvalidPatternEmail = false;
        } else {
            $scope.disabledInvalidPatternEmail = true;
            $scope.disabledGetEmail = true;
        }
    };
    //Fín funciones para validar determinados campos

}]);
