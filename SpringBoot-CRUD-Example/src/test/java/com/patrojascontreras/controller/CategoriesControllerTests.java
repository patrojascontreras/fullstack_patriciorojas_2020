package com.patrojascontreras.controller;

import java.util.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import com.patrojascontreras.controller.CategoriesController;
import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.services.CategoriesService;

@ExtendWith(MockitoExtension.class)
public class CategoriesControllerTests {

    @InjectMocks
    private CategoriesController categoriesController;

    @Mock
    private CategoriesService categoriesService;

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Categorias")
    public void shouldFindCategoriesListAllThenSuccess() {
        //When
        when(categoriesService.categoriesListAll()).thenReturn(Arrays.asList(new Category()));

        ResponseEntity<Object> responseEntity = categoriesController.categoriesListAll();
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ResponseEntity.class, responseEntity.getClass());
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Categorias")
    public void shouldFindCategoriesListAllNotFoundReturn() {
        //When
        when(categoriesService.categoriesListAll()).thenReturn(new ArrayList<>());

        ResponseEntity<Object> responseEntity = categoriesController.categoriesListAll();
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

}
