package com.patrojascontreras.controller;

import java.util.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import com.patrojascontreras.controller.PersonsController;
import com.patrojascontreras.models.dtos.request.PersonCreateRequest;
import com.patrojascontreras.models.dtos.request.PersonUpdateRequest;
import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.services.PersonsService;

@ExtendWith(MockitoExtension.class)
public class PersonsControllerTests {

    @InjectMocks
    private PersonsController personsController;

    @Mock
    private PersonsService personsService;

    //Variable para obtener dato
    private static final int requestId = 122;

    //Variables para crear dato
    private static final String createRequestName = "Alexis";
    private static final String createRequestLastName = "Sanchez";
    private static final String createRequestMale = "Masculino";
    private static final String createRequestEmail = "alexis.sanchez.as7@gmail.com";
    private static final int createRequestCategory = 2;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo";
    private static final String updateRequestLastName = "Vidal";
    private static final String updateRequestMale = "Femenino";
    private static final String updateRequestEmail = "arturo.vidal.carretero.23@gmail.com";
    private static final int updateRequestCategory = 2;

    @Test
    @DisplayName("Metodo de Test para creación de una Persona")
    public void shouldPersonCreateThenSuccess() {
        //Given
        PersonCreateRequest per = givenPersonCreateRequest();
        //When
        Person newPerson = new Person();

        newPerson.setName(per.getName());
        newPerson.setLastName(per.getLastName());
        newPerson.setMale(per.getMale());
        newPerson.setEmail(per.getEmail());
        newPerson.setPersonCategory(per.getPersonCategory());

        when(personsService.insertPerson(any(Person.class))).thenReturn(newPerson);

        ResponseEntity<Object> responseEntity = personsController.insertPerson(per);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
    }

    @Test
    @DisplayName("Metodo de Test para modificación de una Persona mediante retorno no encontrado")
    public void shouldPersonUpdateThenNotFoundReturn() {
        //Given
        Person per = givenPersonCreateNormal();
        per.setIdPerson(requestId);

        PersonUpdateRequest personUpdateRequest = givenPersonUpdateRequest();
        personUpdateRequest.setIdPerson(per.getIdPerson());

        Person updtPer = givenPersonUpdateNormal();
        updtPer.setIdPerson(per.getIdPerson());

        when(personsService.getPersonById(updtPer.getIdPerson())).thenReturn(null);
        //When
        ResponseEntity<Object> responseEntity = personsController.updatePerson(personUpdateRequest);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        assertEquals(HttpStatus.NOT_FOUND, responseEntity.getStatusCode());
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de una Persona por ID")
    public void shouldPersonDeleteByIdThenSuccess() {
        //When
        when(personsService.getPersonById(requestId)).thenReturn(new Person());
        doNothing().when(personsService).personDeleteById(requestId);

        ResponseEntity<Object> responseEntity = personsController.deletePersonById(requestId);
        //Then
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(204);
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        assertEquals(ResponseEntity.class, responseEntity.getClass());
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de una Persona por ID no encontrado")
    public void shouldPersonDeleteByIdThenNotFoundReturn() {
        //When
        when(personsService.getPersonById(anyInt())).thenReturn(null);

        ResponseEntity<Object> responseEntity = personsController.deletePersonById(requestId);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas")
    public void shouldFindPersonsListAllThenSuccess() {
        //When
        when(personsService.personsListAll()).thenReturn(Arrays.asList(new Person()));

        ResponseEntity<Object> responseEntity = personsController.personsListAll();
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ResponseEntity.class, responseEntity.getClass());
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Personas")
    public void shouldFindPersonsListAllThenNotFoundReturn() {
        //When
        when(personsService.personsListAll()).thenReturn(new ArrayList<>());

        ResponseEntity<Object> responseEntity = personsController.personsListAll();
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas por ID")
    public void shouldFindPersonsListAllByIdThenSuccess() {
        //When
        when(personsService.personsListAllById(requestId)).thenReturn(Arrays.asList(new Person()));

        ResponseEntity<Object> responseEntity = personsController.personsListAllById(requestId);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ResponseEntity.class, responseEntity.getClass());
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Personas por ID")
    public void shouldFindPersonsListAllByIdThenNotFoundReturn() {
        //When
        when(personsService.personsListAllById(requestId)).thenReturn(new ArrayList<>());

        ResponseEntity<Object> responseEntity = personsController.personsListAllById(requestId);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

    @Test
    @DisplayName("Metodo de Tests para obtener una Persona por ID")
    public void shouldFindByIdThenReturnPerson() {
        //When
        when(personsService.getPersonById(requestId)).thenReturn(new Person());

        ResponseEntity<Object> responseEntity = personsController.getPersonById(requestId);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(ResponseEntity.class, responseEntity.getClass());
    }

    @Test
    @DisplayName("Metodo de Tests para obtener una Persona por ID no encontrado")
    public void shouldFindByIdThenNotFoundPerson() {
        //When
        when(personsService.getPersonById(anyInt())).thenReturn(null);

        ResponseEntity<Object> responseEntity = personsController.getPersonById(requestId);
        //Then
        assertNotNull(responseEntity.getBody());
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
    }

    private Person givenPersonCreateNormal() {
        Person per = new Person();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private PersonCreateRequest givenPersonCreateRequest() {
        PersonCreateRequest per = new PersonCreateRequest();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private Person givenPersonUpdateNormal() {
        Person per = new Person();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

    private PersonUpdateRequest givenPersonUpdateRequest() {
        PersonUpdateRequest per = new PersonUpdateRequest();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

}
