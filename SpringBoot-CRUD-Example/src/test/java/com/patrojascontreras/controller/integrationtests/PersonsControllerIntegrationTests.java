package com.patrojascontreras.controller.integrationtests;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.patrojascontreras.SpringBootCrudExampleApplication;
import com.patrojascontreras.controller.helpers.HttpHelper;
import com.patrojascontreras.models.dtos.request.PersonCreateRequest;
import com.patrojascontreras.models.dtos.request.PersonUpdateRequest;
import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.repositories.PersonsRepository;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = SpringBootCrudExampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonsControllerIntegrationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private PersonsRepository personsRepository;

    @LocalServerPort
    private int port;

    private static final String urlBase = "http://localhost:%s/SpringBoot-CRUD-Example/api/persons";

    private static final String urlGetPersonsListAll = urlBase + "/read/list";
    private static final String urlGetPersonsListAllById = urlGetPersonsListAll + "/%s";
    private static final String urlGetPersonsInnerJoinListAll = urlBase + "/read/innerjoin/list";
    private static final String urlGetPersonsInnerJoinListAllById = urlGetPersonsInnerJoinListAll + "/%s";
    private static final String urlGetPersonById = urlBase + "/read/edit/%s";
    private static final String urlGetPersonDetailsById = urlBase + "/read/persondetails/%s";
    private static final String urlPersonCreate = urlBase + "/create";
    private static final String urlPersonUpdate = urlBase + "/update";
    private static final String urlPersonDeleteById = urlBase + "/delete/%s";

    //Variable para obtener dato
    private static final int requestId = 999;

    //Variables para crear dato
    private static final String createRequestName = "Alexis";
    private static final String createRequestLastName = "Sanchez";
    private static final String createRequestMale = "Masculino";
    private static final String createRequestEmail = "alexis.sanchez.as7@gmail.com";
    private static final int createRequestCategory = 2;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo";
    private static final String updateRequestLastName = "Vidal";
    private static final String updateRequestMale = "Femenino";
    private static final String updateRequestEmail = "arturo.vidal.carretero.23@gmail.com";
    private static final int updateRequestCategory = 2;

    @Test
    public void testCase01ShouldPersonCreateThenSuccessReturn() {
        //Given
        String url = String.format(urlPersonCreate, port);
        PersonCreateRequest createRequest = givenPersonCreateRequest();
        HttpEntity<PersonCreateRequest> request = HttpHelper.getHttpEntity(createRequest);
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.POST, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.CREATED);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase02ShouldPersonUpdateThenSuccessReturn() {
        //Given
        Person newPerson = new Person();
        newPerson.setName(createRequestName);
        newPerson.setLastName(createRequestLastName);
        newPerson.setMale(createRequestMale);
        newPerson.setEmail(createRequestEmail);
        newPerson.setPersonCategory(createRequestCategory);

        Person createdPerson = personsRepository.save(newPerson);

        Person getPersonById = personsRepository.findByIdPerson(createdPerson.getIdPerson());

        PersonUpdateRequest personUpdateRequest = givenPersonUpdateRequest();
        personUpdateRequest.setIdPerson(getPersonById.getIdPerson());

        String url = String.format(urlPersonUpdate, port);
        HttpEntity<PersonUpdateRequest> request = HttpHelper.getHttpEntity(personUpdateRequest);
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.PUT, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase03ShouldFindPersonByIdThenSuccessReturn() {
        //Given
        Person newPerson = new Person();
        newPerson.setName(createRequestName);
        newPerson.setLastName(createRequestLastName);
        newPerson.setMale(createRequestMale);
        newPerson.setEmail(createRequestEmail);
        newPerson.setPersonCategory(createRequestCategory);

        Person createdPerson = personsRepository.save(newPerson);

        String url = String.format(urlGetPersonById, port, createdPerson.getIdPerson());
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase04ShouldFindPersonDetailsByIdThenSuccessReturn() {
        //Given
        Person newPerson = new Person();
        newPerson.setName(createRequestName);
        newPerson.setLastName(createRequestLastName);
        newPerson.setMale(createRequestMale);
        newPerson.setEmail(createRequestEmail);
        newPerson.setPersonCategory(createRequestCategory);

        Person createdPerson = personsRepository.save(newPerson);

        String url = String.format(urlGetPersonDetailsById, port, createdPerson.getIdPerson());
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase05ShouldPersonDeleteByIdThenSuccessReturn() {
        //Given
        Person newPerson = new Person();
        newPerson.setName(createRequestName);
        newPerson.setLastName(createRequestLastName);
        newPerson.setMale(createRequestMale);
        newPerson.setEmail(createRequestEmail);
        newPerson.setPersonCategory(createRequestCategory);

        Person createdPerson = personsRepository.save(newPerson);

        String url = String.format(urlPersonDeleteById, port, createdPerson.getIdPerson());
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.DELETE, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NO_CONTENT);
    }

    @Test
    public void testCase06ShouldPersonDeleteByIdThenNotFoundReturn() {
        //Given
        String url = String.format(urlPersonDeleteById, port, requestId);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.DELETE, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase07ShouldFindPersonsListAllThenSuccessReturn() {
        //Given
        String url = String.format(urlGetPersonsListAll, port);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase08ShouldFindPersonsListAllInnerJoinThenSuccessReturn() {
        //Given
        String url = String.format(urlGetPersonsInnerJoinListAll, port);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.OK);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase09ShouldFindPersonByIdThenNotFoundReturn() {
        //Given
        String url = String.format(urlGetPersonById, port, requestId);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase10ShouldFindPersonDetailsByIdThenNotFoundReturn() {
        //Given
        String url = String.format(urlGetPersonDetailsById, port, requestId);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase11ShouldFindPersonsListAllByIdThenNotFoundReturn() {
        //Given
        String url = String.format(urlGetPersonsListAllById, port, requestId);
        HttpEntity<String> request = HttpHelper.getHttpEntity();
        //When
        ResponseEntity<Object> response = restTemplate.exchange(url, HttpMethod.GET, request, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
    }

    @Test
    public void testCase12ShouldFindPersonsListAllInnerJoinByIdThenNotFoundReturn() {
        //Given
        String url = String.format(urlGetPersonsInnerJoinListAllById, port, requestId);
        //When
        ResponseEntity<Object> response = restTemplate.getForEntity(url, Object.class);
        //Then
        assertEquals(response.getStatusCode(), HttpStatus.NOT_FOUND);
        assertNotNull(response.getBody());
    }

    private PersonCreateRequest givenPersonCreateRequest() {
        PersonCreateRequest per = new PersonCreateRequest();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private PersonUpdateRequest givenPersonUpdateRequest() {
        PersonUpdateRequest per = new PersonUpdateRequest();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

}
