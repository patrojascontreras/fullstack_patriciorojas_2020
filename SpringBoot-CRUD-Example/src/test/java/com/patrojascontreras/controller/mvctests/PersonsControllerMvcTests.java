package com.patrojascontreras.controller.mvctests;

import java.util.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.patrojascontreras.controller.PersonsController;
import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.services.impl.PersonsServiceImpl;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = PersonsController.class)
public class PersonsControllerMvcTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PersonsServiceImpl personsService;

    //Variable para obtener dato
    private static final int requestId = 122;

    //Variables para crear dato
    private static final String createRequestName = "Alexis";
    private static final String createRequestLastName = "Sanchez";
    private static final String createRequestMale = "Masculino";
    private static final String createRequestEmail = "alexis.sanchez.as7@gmail.com";
    private static final int createRequestCategory = 2;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo";
    private static final String updateRequestLastName = "Vidal";
    private static final String updateRequestMale = "Femenino";
    private static final String updateRequestEmail = "arturo.vidal.carretero.23@gmail.com";
    private static final int updateRequestCategory = 2;

    @Test
    @DisplayName("Metodo de Test para creación de una Persona")
    public void shouldPersonCreateThenSuccess() throws Exception {
        //Given
        Person per = givenPersonCreate();

        //When
        when(personsService.insertPerson(any(Person.class))).thenReturn(per);

        ResultActions response = mockMvc.perform(post("/api/persons/create")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(per)));
        //Then
        response.andExpect(status().isCreated())
                .andDo(print())
                .andExpect(jsonPath("$.idPerson").value(per.getIdPerson()))
                .andExpect(jsonPath("$.name").value(per.getName()))
                .andExpect(jsonPath("$.lastName").value(per.getLastName()))
                .andExpect(jsonPath("$.male").value(per.getMale()))
                .andExpect(jsonPath("$.email").value(per.getEmail()))
                .andExpect(jsonPath("$.personCategory").value(per.getPersonCategory()))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Test para modificación de una Persona")
    public void shouldPersonUpdateThenSuccess() throws Exception {
        //Given
        Person createdPerson = givenPersonCreate();
        Person updatedPerson = givenPersonUpdate();
        updatedPerson.setIdPerson(requestId);

        //When
        when(personsService.getPersonById(requestId)).thenReturn(createdPerson);
        when(personsService.updatePerson(any(Person.class))).thenReturn(updatedPerson);

        ResultActions response = mockMvc.perform(put("/api/persons/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedPerson)));
        //Then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.idPerson").value(updatedPerson.getIdPerson()))
                .andExpect(jsonPath("$.name").value(updatedPerson.getName()))
                .andExpect(jsonPath("$.lastName").value(updatedPerson.getLastName()))
                .andExpect(jsonPath("$.male").value(updatedPerson.getMale()))
                .andExpect(jsonPath("$.email").value(updatedPerson.getEmail()))
                .andExpect(jsonPath("$.personCategory").value(updatedPerson.getPersonCategory()))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Test para modificación de una Persona mediante retorno no encontrado")
    public void shouldPersonUpdateThenNotFoundReturn() throws Exception {
        //Given
        Person personUpdt = givenPersonUpdate();
        personUpdt.setIdPerson(requestId);
        //When
        when(personsService.getPersonById(requestId)).thenReturn(null);
        when(personsService.updatePerson(any(Person.class))).thenReturn(personUpdt);

        ResultActions response = mockMvc.perform(put("/api/persons/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(personUpdt)));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Dato no encontrado"))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de una Persona por ID")
    public void shouldPersonDeleteByIdThenSuccess() throws Exception {
        //When
        when(personsService.getPersonById(requestId)).thenReturn(new Person());
        doNothing().when(personsService).personDeleteById(requestId);

        ResultActions response = mockMvc.perform(delete("/api/persons/delete/{idPerson}", requestId)
                .contentType(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isNoContent())
                .andDo(print());

        verify(personsService, times(1)).getPersonById(requestId);
        verify(personsService, times(1)).personDeleteById(requestId);
        verifyNoMoreInteractions(personsService);
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de una Persona por ID no encontrado")
    public void shouldPersonDeleteByIdThenNotFoundReturn() throws Exception {
        //When
        when(personsService.getPersonById(requestId)).thenReturn(null);

        ResultActions response = mockMvc.perform(delete("/api/persons/delete/{idPerson}", requestId)
                .contentType(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Dato no encontrado"))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas")
    public void shouldFindPersonsListAllThenSuccess() throws Exception {
        //When
        when(personsService.personsListAll()).thenReturn(Arrays.asList(new Person()));

        ResultActions response = mockMvc.perform(get("/api/persons/read/list")
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isOk())
                .andDo(print());

        verify(personsService, times(1)).personsListAll();
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Personas")
    public void shouldFindPersonsListAllThenNotFoundReturn() throws Exception {
        //When
        when(personsService.personsListAll()).thenReturn(new ArrayList<>());

        ResultActions response = mockMvc.perform(get("/api/persons/read/list")
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("No hay datos encontrados"))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas por ID")
    public void shouldFindPersonsListAllByIdThenSuccess() throws Exception {
        //When
        when(personsService.personsListAllById(requestId)).thenReturn(Arrays.asList(new Person()));

        ResultActions response = mockMvc.perform(get("/api/persons/read/list/{idPerson}", requestId)
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isOk())
                .andDo(print());

        verify(personsService, times(1)).personsListAllById(requestId);
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Personas por ID")
    public void shouldFindPersonsListAllByIdThenNotFoundReturn() throws Exception {
        //When
        when(personsService.personsListAllById(requestId)).thenReturn(new ArrayList<>());

        ResultActions response = mockMvc.perform(get("/api/persons/read/list/{idPerson}", requestId)
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("No hay datos encontrados"))
                .andReturn();
    }

    @Test
    @DisplayName("Metodo de Tests para obtener una Persona por ID")
    public void shouldFindByIdThenReturnPerson() throws Exception {
        //Given
        Person per = givenPersonCreate();
        //When
        when(personsService.getPersonById(per.getIdPerson())).thenReturn(per);

        ResultActions response = mockMvc.perform(get("/api/persons/read/edit/{idPerson}", per.getIdPerson()));
        //Then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.idPerson").value(per.getIdPerson()))
                .andExpect(jsonPath("$.name").value(per.getName()))
                .andExpect(jsonPath("$.lastName").value(per.getLastName()))
                .andExpect(jsonPath("$.male").value(per.getMale()))
                .andExpect(jsonPath("$.email").value(per.getEmail()))
                .andExpect(jsonPath("$.personCategory").value(per.getPersonCategory()));
    }

    @Test
    @DisplayName("Metodo de Tests para obtener una Persona por ID no encontrado")
    public void shouldFindByIdThenNotFoundPerson() throws Exception {
        //When
        when(personsService.getPersonById(anyInt())).thenReturn(null);

        ResultActions response = mockMvc.perform(get("/api/persons/read/edit/{idPerson}", requestId));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Dato no encontrado"))
                .andReturn();
    }

    private Person givenPersonCreate() {
        Person per = new Person();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private Person givenPersonUpdate() {
        Person per = new Person();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

}
