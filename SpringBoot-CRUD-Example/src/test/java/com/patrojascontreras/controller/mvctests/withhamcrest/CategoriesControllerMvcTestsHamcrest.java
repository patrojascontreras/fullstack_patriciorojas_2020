package com.patrojascontreras.controller.mvctests.withhamcrest;

import java.util.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.patrojascontreras.controller.CategoriesController;
import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.services.impl.CategoriesServiceImpl;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = CategoriesController.class)
public class CategoriesControllerMvcTestsHamcrest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CategoriesServiceImpl categoriesService;

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todas las Categorias")
    public void shouldFindCategoriesListAllThenSuccess() throws Exception {
        //When
        List<Category> categoriesList = new ArrayList<>(Arrays.asList(new Category()));

        when(categoriesService.categoriesListAll()).thenReturn(categoriesList);

        ResultActions response = mockMvc.perform(get("/api/categories/read/list")
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isOk())
                .andDo(print())
                .andExpect(jsonPath("$.size()", is(categoriesList.size())));
    }

    @Test
    @DisplayName("Metodo de Test para obtener mediante retorno no encontrado en listado de todas las Categorias")
    public void shouldFindCategoriesListAllThenNotFoundReturn() throws Exception {
        //When
        when(categoriesService.categoriesListAll()).thenReturn(new ArrayList<>());

        ResultActions response = mockMvc.perform(get("/api/categories/read/list")
                .accept(MediaType.APPLICATION_JSON));
        //Then
        response.andExpect(status().isNotFound())
                .andDo(print())
                .andExpect(jsonPath("$.message", is("No hay datos encontrados")))
                .andReturn();
    }

}
