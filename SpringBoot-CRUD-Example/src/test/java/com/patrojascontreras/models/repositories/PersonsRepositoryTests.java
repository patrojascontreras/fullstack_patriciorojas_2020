package com.patrojascontreras.models.repositories;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.entities.joins.PersonEntity;
import com.patrojascontreras.models.repositories.PersonsRepository;
import com.patrojascontreras.models.repositories.joins.PersonsEntityRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PersonsRepositoryTests {

    @Autowired
    private PersonsRepository personsRepository;

    @Autowired
    private PersonsEntityRepository personsEntityRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    //Variable para obtener dato
    private static final int requestId = 122;

    //Variables para crear dato
    private static final String createRequestName = "Alexis";
    private static final String createRequestLastName = "Sanchez";
    private static final String createRequestMale = "Masculino";
    private static final String createRequestEmail = "alexis.sanchez.as7@gmail.com";
    private static final int createRequestCategory = 2;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo";
    private static final String updateRequestLastName = "Vidal";
    private static final String updateRequestMale = "Femenino";
    private static final String updateRequestEmail = "arturo.vidal.carretero.23@gmail.com";
    private static final int updateRequestCategory = 2;

    @Test
    public void personCreate() {
        //Given
        Person per = givenPersonCreate();
        //When
        per = personsRepository.save(per);
        //Then
        Person actual = testEntityManager.find(Person.class, per.getIdPerson());
        assertEquals(actual, per);
    }

    @Test
    public void personUpdate() {
        //Given
        Person per = givenPersonCreate();
        per = testEntityManager.persist(per);

        Person updtPerson = givenPersonUpdate();
        updtPerson.setIdPerson(per.getIdPerson());
        //When
        updtPerson = personsRepository.save(updtPerson);
        //Then
        Person actual = testEntityManager.find(Person.class, per.getIdPerson());
        assertEquals(actual, updtPerson);
    }

    @Test
    public void personDeleteById() {
        //Given
        Person per = givenPersonCreate();
        per = testEntityManager.persist(per);
        //When
        personsRepository.deleteById(per.getIdPerson());
        //Then
        Person actual = testEntityManager.find(Person.class, per.getIdPerson());
        assertNull(actual);
    }

    @Test
    public void findPersonById() {
        //Given
        Person per = givenPersonCreate();
        per = testEntityManager.persist(per);
        //When
        Person actual = personsRepository.findByIdPerson(per.getIdPerson());
        //Then
        assertThat(actual).isEqualTo(per);
    }

    @Test
    public void findPersonsListAll() {
        //When
        List<Person> personsListAll = (List<Person>) personsRepository.findAll();
        //Then
        assertThat(personsListAll).size().isGreaterThan(0);
    }

    @Test
    public void findPersonsListAllById() {
        //When
        List<Person> personsListAllById = personsRepository.findAllByIdPerson(requestId);
        //Then
        assertNotNull(personsListAllById);
    }

    @Test
    public void findPersonsInnerJoinListAll() {
        //When
        List<PersonEntity> personsListAll = (List<PersonEntity>) personsEntityRepository.findAll();
        //Then
        assertThat(personsListAll).size().isGreaterThan(0);
    }

    @Test
    public void findPersonsInnerJoinListAllById() {
        //When
        List<PersonEntity> personsListAllById = personsEntityRepository.findAllByIdPerson(requestId);
        //Then
        assertNotNull(personsListAllById);
    }

    private Person givenPersonCreate() {
        Person per = new Person();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private Person givenPersonUpdate() {
        Person per = new Person();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

}
