package com.patrojascontreras.models.repositories;

import java.util.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.repositories.CategoriesRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CategoriesRepositoryTests {

    @Autowired
    private CategoriesRepository categoriesRepository;

    @Autowired
    private TestEntityManager testEntityManager;

    @Test
    public void findCategoriesListAll() {
        List<Category> categoriesListAll = (List<Category>) categoriesRepository.findAll();
        assertThat(categoriesListAll).size().isGreaterThan(0);
    }

}
