package com.patrojascontreras.models.services;

import java.util.*;

import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import com.patrojascontreras.exceptions.ObjectNotFoundException;
import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.entities.joins.PersonEntity;
import com.patrojascontreras.models.repositories.PersonsRepository;
import com.patrojascontreras.models.repositories.joins.PersonsEntityRepository;
import com.patrojascontreras.models.services.impl.PersonsServiceImpl;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PersonsServiceTests {

    @InjectMocks
    private PersonsServiceImpl personsService;

    @Mock
    private PersonsRepository personsRepository;

    @Mock
    private PersonsEntityRepository personsEntityRepository;

    private Optional<Person> optionalPerson;

    //Variable para obtener dato
    private static final int requestId = 122;

    //Variables para crear dato
    private static final String createRequestName = "Alexis";
    private static final String createRequestLastName = "Sanchez";
    private static final String createRequestMale = "Masculino";
    private static final String createRequestEmail = "alexis.sanchez.as7@gmail.com";
    private static final int createRequestCategory = 2;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo";
    private static final String updateRequestLastName = "Vidal";
    private static final String updateRequestMale = "Femenino";
    private static final String updateRequestEmail = "arturo.vidal.carretero.23@gmail.com";
    private static final int updateRequestCategory = 2;

    @Test
    @Order(1)
    @DisplayName("Metodo de Test para creación de una Persona")
    public void shouldPersonCreate() {
        //Given
        Person per = givenPersonCreate();
        //When
        when(personsRepository.findByEmail(per.getEmail())).thenReturn(Optional.empty());

        when(personsRepository.save(per)).thenReturn(per);

        Person savedPerson = personsService.insertPerson(per);
        //Then
        assertThat(savedPerson).isNotNull();
    }

    @Test
    @Order(2)
    @DisplayName("Metodo de Test para modificación de una Persona")
    public void shouldPersonUpdate() {
        //Given
        Person personCreate = givenPersonCreate();

        Person personUpdate = givenPersonUpdate();
        personUpdate.setIdPerson(personCreate.getIdPerson());
        //When
        when(personsRepository.findByIdPerson(personUpdate.getIdPerson())).thenReturn(personUpdate);
        when(personsRepository.save(personUpdate)).thenReturn(personUpdate);
        //Then
        assertEquals(personUpdate, personsService.updatePerson(personUpdate));
    }

    @Test
    @Order(3)
    @DisplayName("Metodo de Test para eliminación de una Persona por ID")
    public void shouldPersonDeleteById() {
        //When
        when(personsRepository.findById(anyInt())).thenReturn(optionalPerson);
        doNothing().when(personsRepository).deleteById(anyInt());
        //Then
        personsService.personDeleteById(requestId);
        verify(personsRepository, times(1)).deleteById(anyInt());
    }

    @Test
    @Order(4)
    @DisplayName("Metodo de Test para eliminación de una Persona por ID con Objeto no encontrado")
    public void shouldPersonDeleteByRutWithObjectNotFoundException() {
        //When
        when(personsRepository.findById(anyInt())).thenThrow(new ObjectNotFoundException("Objeto no encontrado"));
        //Then
        try {
            personsService.personDeleteById(requestId);
        } catch(Exception e) {
            assertEquals(ObjectNotFoundException.class, e.getClass());
            assertEquals("Objeto no encontrado", e.getMessage());
        }
    }

    @Test
    @Order(5)
    @DisplayName("Metodo de Test para obtención de una Persona por ID")
    public void shouldReadPersonById() {
        //When
        Person repositoryGetPersonById = personsRepository.findByIdPerson(anyInt());
        when(repositoryGetPersonById).thenReturn(new Person());

        Person serviceGetPersonById = personsService.getPersonById(requestId);
        //Then
        assertNotNull(serviceGetPersonById);
    }

    @Test
    @Order(6)
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas")
    public void shouldReadPersonsListAll() {
        //When
        when(personsRepository.findAll()).thenReturn(Arrays.asList(new Person()));

        List<Person> personsList = personsService.personsListAll();
        //Then
        assertTrue(!personsList.isEmpty());
        verify(personsRepository, times(1)).findAll();
    }

    @Test
    @Order(7)
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas por ID")
    public void shouldReadPersonsListAllById() {
        ///When
        List<Person> persons = Collections.singletonList(new Person());

        when(personsRepository.findAllByIdPerson(anyInt())).thenReturn(persons);
        //Then
        assertEquals(persons, personsService.personsListAllById(requestId));
    }

    @Test
    @Order(8)
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas con Inner Join")
    public void shouldReadPersonsInnerJoinListAll() {
        //When
        when(personsEntityRepository.findAll()).thenReturn(Arrays.asList(new PersonEntity()));

        List<PersonEntity> personsList = personsService.personsListAllInnerJoin();
        //Then
        assertTrue(!personsList.isEmpty());
        verify(personsEntityRepository, times(1)).findAll();
    }

    @Test
    @Order(9)
    @DisplayName("Metodo de Test para obtener el listado de todas las Personas con Inner Join por ID")
    public void shouldReadPersonsInnerJoinListAllById() {
        ///When
        List<PersonEntity> persons = Collections.singletonList(new PersonEntity());

        when(personsEntityRepository.findAllByIdPerson(anyInt())).thenReturn(persons);
        //Then
        assertEquals(persons, personsService.personsListAllInnerJoinById(requestId));
    }

    private Person givenPersonCreate() {
        Person per = new Person();
        per.setName(createRequestName);
        per.setLastName(createRequestLastName);
        per.setMale(createRequestMale);
        per.setEmail(createRequestEmail);
        per.setPersonCategory(createRequestCategory);
        return per;
    }

    private Person givenPersonUpdate() {
        Person per = new Person();
        per.setName(updateRequestName);
        per.setLastName(updateRequestLastName);
        per.setMale(updateRequestMale);
        per.setEmail(updateRequestEmail);
        per.setPersonCategory(updateRequestCategory);
        return per;
    }

}
