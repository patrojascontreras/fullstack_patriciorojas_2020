package com.patrojascontreras.models.services;

import java.util.*;

import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.repositories.CategoriesRepository;
import com.patrojascontreras.models.services.impl.CategoriesServiceImpl;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CategoriesServiceTests {

    @InjectMocks
    private CategoriesServiceImpl categoriesService;

    @Mock
    private CategoriesRepository categoriesRepository;

    @Test
    @Order(1)
    @DisplayName("Metodo de Test para obtener el listado de todas las Categorías")
    public void shouldFindCategoriesListAll() {
        //When
        List<Category> categoriesList = new ArrayList<>();
        categoriesList.add(new Category());

        List<Category> repositoryCategoriesListAll = categoriesRepository.findAll();
        when(repositoryCategoriesListAll).thenReturn(categoriesList);

        List<Category> serviceCategoriesListAll = categoriesService.categoriesListAll();
        //Then
        assertEquals(serviceCategoriesListAll, categoriesList);
        verify(categoriesRepository).findAll();
    }

}
