package com.patrojascontreras.controller;

import java.util.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.patrojascontreras.models.constants.ResMesConstants;
import com.patrojascontreras.models.dtos.Message;
import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.services.CategoriesService;

@CrossOrigin("http://localhost:3200")
@RestController
@Api(value = "Datos API", description = "Operaciones para procesar datos de una Categoría")
@RequestMapping(value = "/api/categories")
public class CategoriesController {

    private final CategoriesService categoriesService;

    @Autowired
    public CategoriesController(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    @ApiOperation("Mostrar listado de todas las Categorías")
    @RequestMapping(value = "/read/list", method = RequestMethod.GET)
    public ResponseEntity<Object> categoriesListAll() {

        try {
            List<Category> categoriesListAll = categoriesService.categoriesListAll();

            if(categoriesListAll.isEmpty()) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_DATALIST_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(categoriesListAll, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
