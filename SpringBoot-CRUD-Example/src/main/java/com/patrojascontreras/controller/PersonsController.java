package com.patrojascontreras.controller;

import java.util.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.patrojascontreras.models.constants.ResMesConstants;
import com.patrojascontreras.models.dtos.Message;
import com.patrojascontreras.models.dtos.request.PersonCreateRequest;
import com.patrojascontreras.models.dtos.request.PersonUpdateRequest;
import com.patrojascontreras.models.dtos.response.PersonDtoResponse;
import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.entities.joins.PersonEntity;
import com.patrojascontreras.models.services.PersonsService;
import com.patrojascontreras.validations.Validation;

@CrossOrigin("http://localhost:3200")
@RestController
@Api(value = "Datos API", description = "Operaciones para procesar datos de una Persona")
@RequestMapping(value = "/api/persons")
public class PersonsController {

    private final PersonsService personsService;

    @Autowired
    public PersonsController(PersonsService personsService) {
        this.personsService = personsService;
    }

    @ApiOperation("Crear Persona")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ResponseEntity<Object> insertPerson(@RequestBody PersonCreateRequest personCreateRequest) {

        try {
            Validation validation = new Validation();

            if(personCreateRequest.getName() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.NAME_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personCreateRequest.getLastName() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.LAST_NAME_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personCreateRequest.getMale() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.MALE_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personCreateRequest.getEmail() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.EMAIL_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personCreateRequest.getPersonCategory() == 0) {
                return new ResponseEntity<>(new Message(ResMesConstants.CATEGORY_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarNombre(personCreateRequest.getName())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_NAME), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarApellido(personCreateRequest.getLastName())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_LAST_NAME), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarEmail(personCreateRequest.getEmail())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_EMAIL), HttpStatus.BAD_REQUEST);
            }

            Optional<Person> getExistEmailPerson = personsService.getByEmail(personCreateRequest.getEmail());

            if(getExistEmailPerson.isPresent()) {
                return new ResponseEntity<>(new Message(ResMesConstants.EMAIL_PERSON_DATA_EXIST), HttpStatus.OK);
            } else {
                Person per = new Person();

                per.setName(personCreateRequest.getName());
                per.setLastName(personCreateRequest.getLastName());
                per.setMale(personCreateRequest.getMale());
                per.setEmail(personCreateRequest.getEmail());
                per.setPersonCategory(personCreateRequest.getPersonCategory());

                Person createdPer = personsService.insertPerson(per);

                PersonDtoResponse response = new PersonDtoResponse(createdPer.getIdPerson(), createdPer.getName(), createdPer.getLastName(), createdPer.getMale(), createdPer.getEmail(), createdPer.getPersonCategory());

                return new ResponseEntity<>(response, HttpStatus.CREATED);
            }

        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Modificar Persona")
    @RequestMapping(value = "/update", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePerson(@RequestBody PersonUpdateRequest personUpdateRequest) {

        try {
            Validation validation = new Validation();

            if(personUpdateRequest.getIdPerson() == 0) {
                return new ResponseEntity<>(new Message(ResMesConstants.ID_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personUpdateRequest.getName() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.NAME_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personUpdateRequest.getLastName() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.LAST_NAME_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personUpdateRequest.getMale() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.MALE_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personUpdateRequest.getEmail() == "") {
                return new ResponseEntity<>(new Message(ResMesConstants.EMAIL_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(personUpdateRequest.getPersonCategory() == 0) {
                return new ResponseEntity<>(new Message(ResMesConstants.CATEGORY_CAN_NOT_BE_EMPTY), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarNombre(personUpdateRequest.getName())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_NAME), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarApellido(personUpdateRequest.getLastName())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_LAST_NAME), HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarEmail(personUpdateRequest.getEmail())) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_EMAIL), HttpStatus.BAD_REQUEST);
            }

            Person getPersonById = personsService.getPersonById(personUpdateRequest.getIdPerson());

            if(getPersonById == null) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_NOT_FOUND), HttpStatus.NOT_FOUND);
            } else {
                Person per = new Person();

                per.setIdPerson(personUpdateRequest.getIdPerson());
                per.setName(personUpdateRequest.getName());
                per.setLastName(personUpdateRequest.getLastName());
                per.setMale(personUpdateRequest.getMale());
                per.setEmail(personUpdateRequest.getEmail());
                per.setPersonCategory(personUpdateRequest.getPersonCategory());

                Person updatedPer = personsService.updatePerson(per);

                PersonDtoResponse response = new PersonDtoResponse(updatedPer.getIdPerson(), updatedPer.getName(), updatedPer.getLastName(), updatedPer.getMale(), updatedPer.getEmail(), updatedPer.getPersonCategory());

                return new ResponseEntity<>(response, HttpStatus.OK);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Eliminar Persona por ID")
    @RequestMapping(value = "/delete/{idPerson}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> deletePersonById(@PathVariable Integer idPerson) {

        try {
            Person getPersonById = personsService.getPersonById(idPerson);

            if(getPersonById == null) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_NOT_FOUND), HttpStatus.NOT_FOUND);
            } else {
                personsService.personDeleteById(idPerson);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar Persona por ID")
    @RequestMapping(value = "/read/edit/{idPerson}", method = RequestMethod.GET)
    public ResponseEntity<Object> getPersonById(@PathVariable Integer idPerson) {

        try {
            Person getPersonById = personsService.getPersonById(idPerson);

            if(getPersonById != null) {
                Integer id = getPersonById.getIdPerson();
                String name = getPersonById.getName();
                String lastName = getPersonById.getLastName();
                String male = getPersonById.getMale();
                String email = getPersonById.getEmail();
                Integer category = getPersonById.getPersonCategory();

                PersonDtoResponse response = new PersonDtoResponse(id, name, lastName, male, email, category);

                return new ResponseEntity<>(response, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar listado de todas las Personas")
    @RequestMapping(value = "/read/list", method = RequestMethod.GET)
    public ResponseEntity<Object> personsListAll() {

        try {
            List<Person> personsListAll = personsService.personsListAll();

            if(personsListAll.isEmpty()) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_DATALIST_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(personsListAll, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar listado de todas las Personas por ID")
    @RequestMapping(value = "/read/list/{idPerson}", method = RequestMethod.GET)
    public ResponseEntity<Object> personsListAllById(@PathVariable Integer idPerson) {

        try {
            List<Person> personsListAllById = personsService.personsListAllById(idPerson);

            if(personsListAllById.isEmpty()) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_DATALIST_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(personsListAllById, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar listado de todas las Personas mediante Inner Join")
    @RequestMapping(value = "/read/innerjoin/list", method = RequestMethod.GET)
    public ResponseEntity<Object> innerJoinPersonsListAll() {

        try {
            List<PersonEntity> personsListAll = personsService.personsListAllInnerJoin();

            if(personsListAll.isEmpty()) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_DATALIST_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(personsListAll, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar listado de todas las Personas por ID mediante Inner Join")
    @RequestMapping(value = "/read/innerjoin/list/{idPerson}", method = RequestMethod.GET)
    public ResponseEntity<Object> innerJoinPersonsListAllById(@PathVariable Integer idPerson) {

        try {
            List<PersonEntity> personsListAllById = personsService.personsListAllInnerJoinById(idPerson);

            if(personsListAllById.isEmpty()) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_DATALIST_NOT_FOUND), HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(personsListAllById, HttpStatus.OK);
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation("Mostrar detalles de Persona por ID")
    @RequestMapping(value = "/read/persondetails/{idPerson}", method = RequestMethod.GET)
    public ResponseEntity<Object> getPersonDetailsById(@PathVariable Integer idPerson) {

        try {
            PersonEntity getDetailsPersonById = personsService.getPersonDetailsById(idPerson);

            if(getDetailsPersonById == null) {
                return new ResponseEntity<>(new Message(ResMesConstants.RECORD_NOT_FOUND), HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(getDetailsPersonById, HttpStatus.OK);
            }
        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@ApiOperation("Verificar existencia de Persona por Email")
    @RequestMapping(value = "/read/checkemailperson/{email}", method = RequestMethod.GET)
    public ResponseEntity<Object> getPersonExistByEmail(@PathVariable String email) {
        try {
            Validation validation = new Validation();

            if(!validation.validarEmail(email)) {
                return new ResponseEntity<>(new Message(ResMesConstants.INVALID_EMAIL), HttpStatus.BAD_REQUEST);
            }

            Optional<Person> checkEmailExist = personsService.getByEmail(email);

            if(checkEmailExist.isPresent()) {
                return new ResponseEntity<>(new MessageResponse(true, ResMesConstants.EMAIL_PERSON_DATA_EXIST), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(new MessageResponse(false, ""), HttpStatus.NOT_FOUND);
            }

        } catch(Exception e) {
            return new ResponseEntity<>(new Message(ResMesConstants.INTERNAL_SERVER_ERROR_MSG), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
}
