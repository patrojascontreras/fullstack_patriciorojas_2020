package com.patrojascontreras.validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    /**
     * Valida la forma de una dirección de correo
     * @param email cadena de texto con el email a validar
     * @return
     */
    public boolean validarEmail(String email) {
        Pattern pattern = Pattern.compile("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /*
     * Validar Nombre mediante admisión de letras, acentos y espacios
     */
    public boolean validarNombre(String name) {
        Pattern pattern = Pattern.compile("^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\\s]+$");
        Matcher matcher = pattern.matcher(name);
        return matcher.matches();
    }

    /*
     * Validar Apellido(s) mediante admisión de letras, acentos y espacios
     */
    public boolean validarApellido(String lastName) {
        Pattern pattern = Pattern.compile("^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ\\s]+$");
        Matcher matcher = pattern.matcher(lastName);
        return matcher.matches();
    }

}
