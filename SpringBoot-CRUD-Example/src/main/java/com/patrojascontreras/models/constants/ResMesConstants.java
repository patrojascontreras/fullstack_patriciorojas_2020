package com.patrojascontreras.models.constants;

public interface ResMesConstants {
    public final String INTERNAL_SERVER_ERROR_MSG = "Error interno en el servidor";
    public final String RECORD_NOT_FOUND = "Dato no encontrado";
    public final String RECORD_DATALIST_NOT_FOUND = "No hay datos encontrados";
    public final String PERSON_CREATED_SUCCESSFULLY = null;
    public final String PERSON_UPDATED_SUCCESSFULLY = null;
    public final String PERSON_DELETED_SUCCESSFULLY = null;
    public final String ID_CAN_NOT_BE_EMPTY = "El campo ID no puede estar vacío";
    public final String NAME_CAN_NOT_BE_EMPTY = "El campo Nombre no puede estar vacío";
    public final String LAST_NAME_CAN_NOT_BE_EMPTY = "El campo Apellido no puede estar vacío";
    public final String MALE_CAN_NOT_BE_EMPTY = "El campo Sexo no puede estar vacío";
    public final String EMAIL_CAN_NOT_BE_EMPTY = "El campo Email no puede estar vacío";
    public final String CATEGORY_CAN_NOT_BE_EMPTY = "El campo Categoría no puede estar vacío";
    public final String INVALID_NAME = "El campo Nombre está inválido";
    public final String INVALID_LAST_NAME = "El campo Apellido está inválido";
    public final String INVALID_EMAIL = "El campo Email está inválido";
    public final String EMAIL_PERSON_DATA_EXIST = "Ya existe el Email de la persona";
}
