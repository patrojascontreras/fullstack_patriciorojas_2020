package com.patrojascontreras.models.dtos.response;

public class PersonDtoResponse {

    private int idPerson;
    private String name;
    private String lastName;
    private String male;
    private String email;
    private int personCategory;

    public PersonDtoResponse(int idPerson, String name, String lastName, String male, String email, int personCategory) {
        this.idPerson = idPerson;
        this.name = name;
        this.lastName = lastName;
        this.male = male;
        this.email = email;
        this.personCategory = personCategory;
    }

    public int getIdPerson() {
        return idPerson;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMale() {
        return male;
    }

    public String getEmail() {
        return email;
    }

    public int getPersonCategory() {
        return personCategory;
    }

}
