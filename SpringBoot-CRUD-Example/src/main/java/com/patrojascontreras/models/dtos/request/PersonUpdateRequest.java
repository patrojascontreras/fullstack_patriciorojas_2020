package com.patrojascontreras.models.dtos.request;

public class PersonUpdateRequest {

    private int idPerson;
    private String name;
    private String lastName;
    private String male;
    private String email;
    private int personCategory;

    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMale() {
        return male;
    }

    public void setMale(String male) {
        this.male = male;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPersonCategory() {
        return personCategory;
    }

    public void setPersonCategory(int personCategory) {
        this.personCategory = personCategory;
    }

}
