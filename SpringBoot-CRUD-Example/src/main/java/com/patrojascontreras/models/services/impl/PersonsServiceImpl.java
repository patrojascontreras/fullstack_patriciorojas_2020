package com.patrojascontreras.models.services.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.entities.joins.PersonEntity;
import com.patrojascontreras.models.repositories.PersonsRepository;
import com.patrojascontreras.models.repositories.joins.PersonsEntityRepository;
import com.patrojascontreras.models.services.PersonsService;

@Service
public class PersonsServiceImpl implements PersonsService {

    private final PersonsRepository personsRepository;

    private final PersonsEntityRepository personsEntityRepository;

    @Autowired
    public PersonsServiceImpl(PersonsRepository personsRepository, PersonsEntityRepository personsEntityRepository) {
        this.personsRepository = personsRepository;
        this.personsEntityRepository = personsEntityRepository;
    }

    public Person insertPerson(Person person) {
        return personsRepository.save(person);
    }

    public Person updatePerson(Person person) {
        return personsRepository.save(person);
    }

    public void personDeleteById(Integer idPerson) {
        personsRepository.deleteById(idPerson);
    }

    public List<Person> personsListAll() {
        return personsRepository.findAll();
    }

    public List<Person> personsListAllById(Integer idPerson) {
        return personsRepository.findAllByIdPerson(idPerson);
    }

    public Person getPersonById(Integer idPerson) {
        return personsRepository.findByIdPerson(idPerson);
    }

    public Optional<Person> getByEmail(String email) {
        return personsRepository.findByEmail(email);
    }

    public List<PersonEntity> personsListAllInnerJoin() {
        return personsEntityRepository.findAll();
    }

    public List<PersonEntity> personsListAllInnerJoinById(Integer idPerson) {
        return personsEntityRepository.findAllByIdPerson(idPerson);
    }

    public PersonEntity getPersonDetailsById(Integer idPerson) {
        return personsEntityRepository.findByIdPerson(idPerson);
    }

}
