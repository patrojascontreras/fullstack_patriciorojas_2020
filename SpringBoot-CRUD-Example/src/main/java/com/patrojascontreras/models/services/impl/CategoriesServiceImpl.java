package com.patrojascontreras.models.services.impl;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patrojascontreras.models.entities.Category;
import com.patrojascontreras.models.repositories.CategoriesRepository;
import com.patrojascontreras.models.services.CategoriesService;

@Service
public class CategoriesServiceImpl implements CategoriesService {

    private final CategoriesRepository categoriesRepository;

    @Autowired
    public CategoriesServiceImpl(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }

    public List<Category> categoriesListAll() {
        return categoriesRepository.findAll();
    }

}
