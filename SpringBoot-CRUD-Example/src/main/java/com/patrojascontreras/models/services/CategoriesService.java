package com.patrojascontreras.models.services;

import java.util.*;

import com.patrojascontreras.models.entities.Category;

public interface CategoriesService {

    public List<Category> categoriesListAll();
}
