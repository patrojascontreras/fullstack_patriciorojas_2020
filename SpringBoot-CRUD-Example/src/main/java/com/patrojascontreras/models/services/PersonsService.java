package com.patrojascontreras.models.services;

import java.util.*;

import com.patrojascontreras.models.entities.Person;
import com.patrojascontreras.models.entities.joins.PersonEntity;

public interface PersonsService {

    public Person insertPerson(Person person);
    public Person updatePerson(Person person);
    public void personDeleteById(Integer idPerson);
    public List<Person> personsListAll();
    public List<Person> personsListAllById(Integer idPerson);
    public Person getPersonById(Integer idPerson);
    public Optional<Person> getByEmail(String email);
    public List<PersonEntity> personsListAllInnerJoin();
    public List<PersonEntity> personsListAllInnerJoinById(Integer idPerson);
    public PersonEntity getPersonDetailsById(Integer idPerson);
}
