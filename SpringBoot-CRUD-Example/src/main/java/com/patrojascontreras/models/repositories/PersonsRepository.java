package com.patrojascontreras.models.repositories;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.patrojascontreras.models.entities.Person;

@Repository
public interface PersonsRepository extends JpaRepository<Person, Integer> {

    public Person findByIdPerson(Integer idPerson);
    public Optional<Person> findByEmail(String email);
    public List<Person> findAllByIdPerson(Integer idPerson);
}
