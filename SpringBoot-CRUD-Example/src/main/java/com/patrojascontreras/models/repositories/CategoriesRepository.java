package com.patrojascontreras.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.patrojascontreras.models.entities.Category;

@Repository
public interface CategoriesRepository extends JpaRepository<Category, Integer> {

}
