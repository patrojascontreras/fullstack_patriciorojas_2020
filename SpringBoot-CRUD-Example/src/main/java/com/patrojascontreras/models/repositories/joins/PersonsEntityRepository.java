package com.patrojascontreras.models.repositories.joins;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.patrojascontreras.models.entities.joins.PersonEntity;

@Repository
public interface PersonsEntityRepository extends JpaRepository<PersonEntity, Integer> {

    @Query(value = "SELECT p.id_person, p.name, p.last_name, p.male, p.email, c.category_name FROM person p INNER JOIN category c ON c.id_category = p.person_category", nativeQuery = true)
    public List<PersonEntity> findAll();

    @Query(value = "SELECT p.id_person, p.name, p.last_name, p.male, p.email, c.category_name FROM person p INNER JOIN category c ON c.id_category = p.person_category WHERE p.id_person = ?1", nativeQuery = true)
    public List<PersonEntity> findAllByIdPerson(Integer idPerson);

    @Query(value = "SELECT p.id_person, p.name, p.last_name, p.male, p.email, c.category_name FROM person p INNER JOIN category c ON c.id_category = p.person_category WHERE p.id_person = ?1", nativeQuery = true)
    public PersonEntity findByIdPerson(Integer idPerson);
}
