# FullStack_PatricioRojas_2020



## Descripción

Proyecto de Desarrollo Web sobre un CRUD de gestión de Personas realizado a traves de tecnologías demandadas en el mercado mediante BackEnd y FrontEnd. 

#### Diagrama de Arquitectura de la aplicación: 

![Diagrama de Arquitectura de la Aplicación](/Images/img_diagrama_arquitectura_proyecto_fullstack.jpg) 

### BackEnd

Proyecto en Java 11 fabricado para el consumo de servicios REST mediante el uso de tecnologías basadas en Spring Framework 5 a traves de Spring Boot en conjunto con Hibernate y JPA (Java Persistence API), que cumple con la funcionalidad de procesar a traves de operaciones CRUD (Lo cuál significa Create, Read, Update and Delete, estas son las cuatro operaciones básicas que se realizan en el almacenamiento persistente).

Entonces, las operaciones CRUD estándar son las siguientes:

* **POST :** Crea un nuevo recurso.
* **GET :** Lee/recupera un recurso.
* **PUT :** Actualiza un recurso existente.
* **DELETE :** Elimina un recurso.

**Aviso importante:** El BackEnd de este proyecto con las tecnologías previamente mencionadas fue originalmente modificado en mayo de 2024 debido a razones de tiempo, a pesar de haberse aprendido y realizado en 2020 durante el proceso de la pandemia del COVID-19. 

#### Diagrama de Casos de Uso del Proyecto: 

![Diagrama de Casos de Uso](/Images/img_diagrama_casos_de_uso_uml_proyecto_crud.jpg) 

**Descripción:** El diagrama de casos de uso que se está mostrando consiste en la especificación de los procesos CRUD que debe realizar un cliente al momento de ingresar a la aplicación en relación a la gestión de personas. 

#### Diagrama de Arquitectura de alto nivel del proyecto:

![Diagrama de Arquitectura](/Images/img_diagrama_arquitectura_proyecto.png)

El diagrama de arquitectura de alto nivel representa el flujo de datos y las interacciones dentro de la aplicación Spring Boot. Ilustra cómo la interfaz de usuario/Postman se comunica con la API Spring Boot, que se coordina aún más con la capa de servicio y Spring Data JPA para realizar operaciones CRUD en la base de datos MySQL.

#### Estructura del proyecto:

![Estructura del proyecto](/Images/springboot-projectstructure.jpg)

El proyecto contiene Pruebas unitarias y de integración para los API's REST que están fabricados con Spring Boot mediante el uso de Spring JUnit 5 en conjunto con Mockito y Hamcrest, los cuales son frameworks vinculados a las pruebas de tests unitarios.

Tambien se ha implementado JaCoCo para la cobertura de pruebas en el código, que el cuál se puede mostrar el reporte a traves de un archivo html usando el siguiente proceso: C:\SpringBoot-CRUD-Example\target\site\jacoco\index.html

#### Reporte de JaCoCo en el proyecto:

![Reporte de JaCoCo en el proyecto](/Images/springboot-jacoco-report.jpg)

**Aviso importante:** Las pruebas unitarias que se han agregado a este proyecto, se han obtenido a traves del curso "Testes com JUnit 5 Mockito e Spring Boot (REST APIs)" que he estado realizando sobre la plataforma Udemy, que el cuál he logrado aprobar después de haberlo estado realizando durante 5 meses por motivos de tiempo. Url de la certificación: https://www.udemy.com/certificate/UC-06baddc4-ab99-45f1-911c-b0af007f5b5a/

Uso de **Patrones de Diseño** en el proyecto:

* **Adapter    :** Es un patrón de tipo estructural que cumple con la funcionalidad de permitir la colaboración entre objetos con interfaces incompatibles. 
    
	En Spring Boot, los adaptadores se pueden implementar como controladores para adaptar las llamadas HTTP a la lógica de negocio. 
    ![Ejemplo Adapter](/Images/SpringBootProject-DesignPattern-AdapterExample.jpg) 
* **Repository :** Es un patrón de diseño que se utiliza para separar la lógica de acceso a datos de la lógica de negocio en una aplicación. 

    Dentro de Spring Boot, los repositorios se pueden implementar fácilmente usando Spring Data JPA. 
	
	Definiendo una interfaz que extiende JpaRepository, Spring generará automáticamente la implementación en tiempo de ejecución. 
    ![Ejemplo Repository](/Images/SpringBootProject-DesignPattern-RepositoryExample.jpg) 
	
* **Singleton :** Es un patrón de diseño creacional que garantiza que tan solo exista un objeto de su tipo y proporciona un único punto de acceso a él para cualquier otro código. 
    
	![Ejemplo Singleton a nivel general](/Images/singleton_example_class.jpg) 
	
	Dentro de Java y todo lo relacionado a Spring, este tipo de patrón suele utilizarse en clase que están relacionadas a temas de configuración de parametros generales de la aplicación ya que una vez instanciado el objeto los valores se mantienen y son compartidos por toda la aplicación. **Ejemplo:** Una clase de Conexión a una base de datos determinada. 
	
	Dentro de este proyecto, el patrón Singleton se está utilizando en una clase que está vinculada a una configuración determinada que aparece en la siguiente imagen: 
	![Ejemplo Singleton](/Images/SpringBootProject-DesignPattern-SingletonExample.jpg) 
	
Uso de **Arquitectura Limpia** (En inglés: **Clean Architecture**) mediante el estilo de arquitectura llamado **Organización de Código** (En inglés: **Organizing Code**) denonimado **En capas** (En ingles: **Layered**):  

Esta es la forma más sencilla de organizar paquetes, ya que cada paquete se define por su responsabilidad. Sin embargo, puede que no sea el mejor enfoque para organizar proyectos complejos. No obstante, se utiliza habitualmente para proyectos pequeños y la validación debe aplicarse en cada capa. 

Para llevarse a cabo esta forma de organizar código dentro de un proyecto, se puede organizar de la siguiente manera: 

![Imagen Layered - Arquitectura Limpia](/Images/clean-architecture_organization-code_layered_example.jpg) 

##### Definición de Arquitectura Limpia: 

La arquitectura limpia es un principio de diseño de software introducido por Robert C. Martin, a menudo conocido como “el tío Bob”. Es una forma de organizar el código y definir la arquitectura de una aplicación de software para lograr una mejor capacidad de mantenimiento, flexibilidad y capacidad de prueba. 

![Imagen Structure - Arquitectura Limpia](/Images/clean-architecture_projects_structure.jpg) 

![Imagen Structure Details - Arquitectura Limpia](/Images/clean-architecture_mvc_projects_structure_details.jpg) 

**Beneficios:** 

Puede escribir pruebas unitarias para casos de uso sin necesidad de trabajar con bases de datos o componentes de la interfaz de usuario. Mantenibilidad: la arquitectura limpia facilita la modificación y ampliación de la aplicación sin afectar a otras partes. Los cambios en los adaptadores externos no afectan al núcleo interno. 

Dentro del proyecto, tambien se están utilizando uno de los siguientes **principios S.O.L.I.D**:
* **D - Dependency Inversion Principle (Principio de Inversión de Dependencia)      :** Es un principio que se encarga de establecer que una clase no debe depender de clases concretas, sino de abstracciones. En otras palabras, no debemos crear dependencias sobre clases específicas, sino sobre interfaces o clases abstractas. 
    
    Por ejemplo, digamos que tenemos una clase que depende de una clase de base de datos concreta. Si necesitamos cambiar la base de datos, también necesitaremos cambiar el código de la clase. Esto viola el principio de inversión de dependencia. 
    
    Una mejor manera de diseñar esto sería crear una dependencia en una clase de base de datos abstracta. Entonces podemos crear una clase MysqlDatabase concreta que amplíe la base de datos. De esta forma, podemos cambiar la base de datos sin tener que cambiar el código de la clase.
	
	**Ejemplos DIP con Spring Boot:** 
    ![Principio Inversión de Dependencia - Ejemplo Incorrecto](/Images/SOLID-principles_Dependency-Inversion-Principle_incorrect-example.jpg) 
    ![Principio Inversión de Dependencia - Ejemplo Correcto](/Images/SOLID-principles_Dependency-Inversion-Principle_correct-example.jpg) 
	
##### Definición de Principios S.O.L.I.D: 

Principios SOLID se define como un conjunto de principios que se utilizan en el desarrollo de software para hacer que los diseños sean más comprensibles, flexibles, escalables y resistentes a los cambios. 

Los Principios Solid indican cómo organizar funciones y estructuras de datos en componentes y cómo dichos componentes deben estar interconectados. 

Normalmente estos componentes suelen ser clases, aunque esto no implica que dichos principios sólo sean aplicables a ellas. Por el contrario, los principios SOLID se pueden aplicar a cada producto de software. 

SOLID es un acrónimo que representa cinco principios utilizados por ingenieros de software y desarrolladores. 

Los principios son los siguientes: 

![Imagen Resumen Principios S.O.L.I.D](/Images/principios-solid_resumen.jpg) 

En este proyecto, también se está utilizando **Arquitectura Monolítica**, la cuál se define como un modelo tradicional de un programa de software que se compila como una unidad unificada y que es autónoma e independiente de otras aplicaciones. 

![Imagen Definición Arquitectura Monolítica](/Images/img_arquitectura_monolitica.jpg) 

Proyecto de tipo Maven.

Conexión a Base de Datos Relacional MySQL. 

#### Modelo de la Base de Datos (MER o Modelo Entidad-Relación): 

A través del siguiente modelo, se preparó la Base de Datos Relacional del proyecto. 

![Modelo de la Base de Datos](/Images/mer_database_crud_example_project.jpg) 

#### Configuración local:

1. Instalación limpia en proyecto Maven: **mvn clean install**
2. Empaquetación limpia del código compilado en su formato distribuible: **mvn clean package**

#### Configuración de la Base de Datos en el proyecto:

![Configuración de la Base de Datos en el proyecto](/Images/springboot-databaseconnection-configuration.jpg)

#### Ejecución de la aplicación:

Con respecto a las aplicaciones fabricadas con Spring Boot, existen dos formas de iniciar su ejecución:

1. Desde el directorio raíz de la aplicación a traves de siguiente comando: **mvn spring-boot:run**

2. Desde el IDE que en el cuál se está desarrollando la aplicación seleccionando el metodo Main del mismo proyecto:

* Paso 01:

![Segunda opción de compilación 01](/Images/springboot-deploymentprocess-001.jpg)

* Paso 02:

![Segunda opción de compilación 02](/Images/springboot-deploymentprocess-002.jpg)

### Documentación de Endpoints:

Uso de documentación Swagger UI, ingresando a traves de la siguiente url: https://localhost:8080/SpringBoot-CRUD-Example/swagger-ui.html

![Pantallazo documentación de API's Swagger UI](/Images/springboot-swagger-documentation-apis.jpg)

### Ejecución de Testing:

Ejecutar tests unitarios: **> mvn test** 

**Aviso importante:** 

En relación a una parte del aprendizaje y comprensión de los **Patrones de Diseño** y los **Principios S.O.L.I.D**, se aplicaron en parte gracias a la realización de las siguientes certificaciones en 2023, aunque se hayan comprendido de una manera más detallada a través del tiempo: 

* **Patrones de Diseño de software y principios SOLID** - Url de Certificado: https://www.udemy.com/certificate/UC-c1fa9dc9-dd50-4d7b-9ca0-867fee914fe1/ 
* **Patrones de Diseño Java** - Url de Certificado: https://www.udemy.com/certificate/UC-aa173c54-7272-4231-9ef4-e048e59086cf/ 

### FrontEnd

Uso de tecnologías como HTML5, CSS3, JavaScript en conjunto con las librerías JQuery, Ajax y Toastr, y también con los Framework AngularJs y Bootstrap. 

**Avisos importantes:** 

* Uso de patrón de diseño o arquitectura SPA (Single-Page Application, en español:  Aplicación de página única) en el proyecto FrontEnd. 
* También se ha implementado el patrón MVC (Modelo Vista Controlador) mediante el uso de JavaScript y HTML. La vista se define en HTML, mientras que el modelo y el controlador se implementan en JavaScript. 

#### Estructura del proyecto: 

![Estructura del proyecto FrontEnd](/Images/angularjs-crud-example_img_estructura_proyecto_frontend.jpg) 

Para la creación y ejecución del proyecto en AngularJs, se procedió de la siguiente manera a traves de CMD mediante comandos:

1) Creación de directorio vinculado al proyecto: **mkdir AngularJs-CRUD-Example**
2) Ingresar al proyecto mediante el uso de un IDE como VS Code.
3) Creación de archivos HTML, css y js vinculados al proyecto, y realizar el desarrollo correspondiente.
4) Hacer despliegue del proyecto en el archivo index.html para hacer la prueba de que esté funcionando el desarrollo previo.

![Evidencia 01](/Images/angularjs-crud-example_img_paso01-0001.jpg)

![Evidencia 02](/Images/angularjs-crud-example_img_paso01-0002.jpg) 

5) Retornar a CMD y crear el archivo package.json mediante ejecución del siguiente comando: **npm init** 
6) Instalar las siguientes dependencias:
* **Paso 01:** npm install 
* **Paso 02:** npm install --save express 
* **Paso 03:** npm install morgan --save
7) Creación de archivo index.js para la interacción del Framework NodeJs en el mismo proyecto.
8) Ejecutar sgte comando para desplegar el proyecto: **npm start**  

![Evidencia 03](/Images/deployment_angularjs_crud_example_project_using_nodejs.jpg) 

9) Ingresar a url para mostrar el despliegue exitoso del proyecto: http://localhost:3200 

**Evidencia sin interacción con BackEnd** 
![Evidencia 04](/Images/angularjs-crud-example_img_paso02-0002_despliegueproyecto.jpg)

**Evidencia usando interacción entre BackEnd y FrontEnd** 
![Evidencia 05](/Images/angularjs-crud-example_img_paso02-0002_despliegueproyecto_backfront.jpg) 

### Glosario 

* **API:** Una API (acrónimo que significa Interfaz de Programación de Aplicaciones - En inglés: Application Programming Interface) es una interfaz que permite que diferentes programas de ordenador se comuniquen entre sí.
* **API REST:** Es un tipo de API que sigue un conjunto de reglas y estándares que la hacen más fácil de usar y más compatible con otras aplicaciones. 
            
    Tambien se puede definir que una API REST, o API RESTful, es una interfaz de programación de aplicaciones (API o API web) que se ajusta a los límites de la arquitectura REST y permite la interacción con los servicios web de RESTful.
* **Arquitectura de microservicios:** Se define como una metodología de diseño para una rápida implementación y actualización de las aplicaciones basadas en la nube. Los microservicios suelen operar de forma independiente, a menudo en contenedores con acople suelto de una interfaz estándar.
* **Arquitectura de software:** Es un concepto que surge ya en los años 60 y se refiere a una planificación basada en modelos, patrones y abstracciones teóricas, a la hora de realizar una pieza de software de cierta complejidad y como paso previo a cualquier implementación.
* **Diseño del software:** El diseño de software es el proceso de visionado y definición de soluciones software a uno o más conjuntos de problemas. Uno de los componentes principales del diseño de software es la especificación de requisitos del software (ERS).
* **Microservicios:** Es una forma de desarrollo de software que divide una aplicación en pequeños servicios independientes, donde cada uno se ejecuta en su propio proceso y se puede implementar, escalar y mantener por sí mismo. Cabe destacar que el término de microservicios se refiere tanto a un estilo de arquitectura como a un modo de programar software. 
    
    Por ello, los microservicios se definen como una manera de programar software o desarrollar estilos de arquitecturas; sus implementaciones se dividen en elementos independientes entre sí, que a diferencia del enfoque tradicional de cada aplicación en donde todo se compila en una sola pieza, los microservicios son piezas autónomas que operan en conjunto para llevar a cabo las mismas tareas. 
* **POJO:** Es un acrónimo de Plain Old Java Object que es utilizado por programadores Java para enfatizar el uso de clases simples y que no dependen de un framework en especial. 
    
    Tambien se define que un objeto POJO es una instancia de una clase que no extiende ni implementa nada en especial. Por ejemplo, un Servlet tiene que extender de HttpServlet y sobrescribir sus métodos, por lo tanto no es un POJO. 
  
Realizado por Ing. Patricio Rojas Contreras - Julio de 2020.
